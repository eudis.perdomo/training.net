﻿// See https://aka.ms/new-console-template for more information
using PrintFramerAPICLient;

Console.WriteLine("Hello, World!");

var httpClient = new HttpClient();
var clientSwagger = new swaggerClient("https://localhost:64236/", httpClient);

var result = await clientSwagger.GetPriceAsync("23", "34");
Console.WriteLine(result);
Console.ReadKey();